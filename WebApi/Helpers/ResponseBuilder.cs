﻿using System;
using System.Net;

namespace WebApi.Helpers
{
    /// <summary>
    /// Class that builds nice looking responses
    /// </summary>
    public class ResponseBuilder
    {
        public string Message { get; }
        public DateTime Time { get; }
        public HttpStatusCode Code { get; }

        public ResponseBuilder(string message, HttpStatusCode code)
        {
            Message = message;
            Time = DateTime.Now;
            Code = code;
        }
    }
}
