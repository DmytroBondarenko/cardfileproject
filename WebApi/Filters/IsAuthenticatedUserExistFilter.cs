﻿using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Business.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using WebApi.Helpers;

namespace WebApi.Filters
{
    public class IsAuthenticatedUserExistFilter : IAsyncActionFilter
    {
        private readonly IUserProfileService _userProfileService;

        public IsAuthenticatedUserExistFilter(IUserProfileService userProfileService)
        {
            _userProfileService = userProfileService;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (await _userProfileService.GetUserProfileById(
                context.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value) == null)
            {
                context.Result = new BadRequestObjectResult(new ResponseBuilder(
                    "Something went wrong. Maybe your account was deleted", HttpStatusCode.BadRequest));
            }
            else
            {
                await next();
            }
        }
    }
}
