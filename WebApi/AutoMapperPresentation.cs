﻿using AutoMapper;
using Business.Dtos;
using WebApi.Models;

namespace WebApi
{
    /// <summary>
    /// AutoMapper to map API models with DTOs
    /// </summary>
    public class AutoMapperPresentation : Profile
    {
        public AutoMapperPresentation()
        {
            CreateMap<UserDto, RegisterModel>()
                .ReverseMap();

            CreateMap<TextDto, TextModel>()
                .ForMember(d => d.GenreIds, c => c.MapFrom(u => u.GenreIds))
                .ReverseMap();

            CreateMap<UserDto, UpdateUserModel>()
                .ReverseMap();

            CreateMap<GenreDto, GenreModel>().ReverseMap();

            CreateMap<UserProfileModel, UserProfileDto>().ReverseMap();
        }
    }
}
