﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{
    public class TextModel
    {
        [Required]
        [StringLength(100, MinimumLength = 3)]
        public string Title { get; set; }

        [Required]
        [StringLength(1500, MinimumLength = 15)]
        public string Body { get; set; }

        [Required]
        [MinLength(1, ErrorMessage = "Text must have at least one genre")]
        public ICollection<Guid> GenreIds { get; set; }
    }
}
