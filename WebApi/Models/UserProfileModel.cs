﻿using System;
using System.Collections.Generic;

namespace WebApi.Models
{
    public class UserProfileModel
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public virtual ICollection<Guid> TextIds { get; set; }
    }
}
