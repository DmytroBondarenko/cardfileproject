﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using AutoMapper;
using Business.Dtos;
using Business.Interfaces;
using Microsoft.AspNetCore.Http;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public AuthController(IMapper mapper, IAuthService authService, IUserService userService)
        {
            _authService = authService;
            _mapper = mapper;
            _userService = userService;
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegisterModel model)
        {
            return new ObjectResult(await _authService.RegisterUser(_mapper.Map<UserDto>(model), model.Password)) 
                { StatusCode = StatusCodes.Status201Created };
        }

        /// <summary>
        /// action for user login
        /// </summary>
        /// <param name="model">user login model with email and password</param>
        /// <returns>Ok if user is authorized successfully, otherwise - Unauthorized</returns>
        [HttpPost]
        [Route("login")]
        public async Task<ActionResult> Login([FromBody] LoginModel model)
        {
            var user = await _userService.GetUserByEmail(model.Email);

            if (user == null || !await _userService.UserCheckPassword(model.Email, model.Password))
                return Unauthorized();

            var loginResult = await _authService.LoginUser(user);
            return Ok(loginResult);
        }
    }
}
