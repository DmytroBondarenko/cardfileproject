﻿using Microsoft.AspNetCore.Mvc;
using System.Net;
using Business.Exceptions;
using Microsoft.AspNetCore.Diagnostics;
using WebApi.Helpers;

namespace WebApi.Controllers
{
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ErrorController : ControllerBase
    {
        [Route("/error")]
        public ActionResult Error()
        {
            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();
            var exception = context?.Error;

            if (exception is CardFileException)
                return BadRequest(new ResponseBuilder(exception.Message, HttpStatusCode.BadRequest));

            if(exception is NotFoundException)
                return NotFound(new ResponseBuilder(exception.Message, HttpStatusCode.NotFound));

            return new JsonResult(new ResponseBuilder("Something went wrong.", HttpStatusCode.InternalServerError));
        }
    }
}
