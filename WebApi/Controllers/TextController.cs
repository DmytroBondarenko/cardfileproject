﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Business.Constants;
using Business.Dtos;
using Business.Interfaces;
using Microsoft.AspNetCore.Authorization;
using WebApi.Filters;
using WebApi.Helpers;
using WebApi.Models;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme)]
    public class TextController : ControllerBase
    {
        private readonly ITextService _textService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public TextController(ITextService textService, IMapper mapper, IUserService userService)
        {
            _textService = textService;
            _mapper = mapper;
            _userService = userService;
        }

        [HttpGet]
        [Route("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<TextDto>>> GetTextById(string id)
        {
            var text = await _textService.GetTextByIdWithDetails(id);
            if (text == null)
            {
                return NotFound();
            }
            return Ok(text);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<TextDto>>> GetAllTexts()
        {
            return Ok(await _textService.GetAllTextsWithDetails());
        }

        [HttpGet]
        [Route("SortByAscending")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<TextDto>>> GetAllTextsAscending()
        {
            return Ok(await _textService.SortTextsByRatingAscending());
        }

        [HttpGet]
        [Route("SortByDescending")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<TextDto>>> GetAllTextsDescending()
        {
            return Ok(await _textService.SortTextsByRatingDescending());
        }

        [HttpGet]
        [Route("UserTexts/{userName}")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<TextDto>>> GetAllTextsByUserName(string userName)
        {
            return Ok(await _textService.GetAllTextsByUserName(userName));
        }

        [HttpGet]
        [Route("GetYourTexts")]
        [Authorize(Roles = Roles.User)]
        [ServiceFilter(typeof(IsAuthenticatedUserExistFilter))]
        public async Task<ActionResult<IEnumerable<TextDto>>> GetAllTextsOfUser()
        {
            return Ok(await _textService.GetAllTextsByUserId(User.FindFirst(ClaimTypes.NameIdentifier).Value));
        }

        [HttpGet]
        [Route("GetByGenre")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<TextDto>>> GetTextsByGenreName([FromQuery] string name)
        {
            return Ok(await _textService.GetAllTextsByGenreName(name));
        }

        [HttpPost]
        [Route("RateText")]
        [Authorize(Roles = Roles.User)]
        [ServiceFilter(typeof(IsAuthenticatedUserExistFilter))]
        public async Task<ActionResult> RateText(string textId)
        {
            await _textService.RateText(textId, User.FindFirst(ClaimTypes.NameIdentifier).Value);
            return NoContent();
        }

        [HttpPost]
        [Route("AddText")]
        [Authorize(Roles = Roles.User)]
        [ServiceFilter(typeof(IsAuthenticatedUserExistFilter))]
        public async Task<ActionResult<TextDto>> AddText([FromBody] TextModel model)
        {
            var textToAdd = _mapper.Map<TextDto>(model);
            textToAdd.UserId = new Guid(User.FindFirst(ClaimTypes.NameIdentifier).Value);
            var timeNow = DateTime.Now;
            textToAdd.PublishDate = timeNow;
            var idOfText = await _textService.AddText(textToAdd);
            var newText = await _textService.GetTextByIdWithDetails(idOfText);
            return CreatedAtAction(nameof(GetTextById), new { id = idOfText }, newText);
        }

        [HttpDelete]
        [Route("DeleteText/{id}")]
        [Authorize(Roles = Roles.User)]
        [ServiceFilter(typeof(IsAuthenticatedUserExistFilter))]
        public async Task<ActionResult> DeleteTextById(string id)
        {
            var roles = await _userService.GetUserRoles(User.FindFirst(ClaimTypes.NameIdentifier)
                .Value);
            if (roles.Any(x => x == Roles.Admin))
            {
                await _textService.DeleteTextById(id);
                return NoContent();
            }

            var textToDelete = await _textService.GetTextByIdWithDetails(id);
            if (textToDelete == null)
            {
                return NotFound(new ResponseBuilder("Text with passed id does not exist", HttpStatusCode.NotFound));
            }

            if (string.CompareOrdinal(
                textToDelete.UserId.ToString(), User.FindFirst(ClaimTypes.NameIdentifier).Value) != 0)
                return BadRequest(new ResponseBuilder("Text you are trying to delete is not yours",
                    HttpStatusCode.BadRequest));
            await _textService.DeleteTextById(id);
            return NoContent();
        }
    }
}
