﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Business.Constants;
using Business.Dtos;
using Business.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using WebApi.Filters;
using WebApi.Helpers;
using WebApi.Models;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme)]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IUserProfileService _userProfileService;
        private readonly IMapper _mapper;

        public UserController(IMapper mapper, IUserService userService, 
            IUserProfileService userProfileService)
        {
            _userService = userService;
            _userProfileService = userProfileService;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("GetAllUserProfiles")]
        [Authorize(Roles = Roles.Admin)]
        public async Task<IEnumerable<UserProfileDto>> GetAllUserProfiles()
        {
            return await _userProfileService.GetAllUserProfilesWithDetails();
        }

        [HttpGet]
        [Route("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<UserProfileModel>> GetUserProfileById(string id)
        {
            var user = await _userProfileService.GetUserProfileByIdWithDetails(id);
            if (user == null) 
                return NotFound();
            return Ok(_mapper.Map<UserProfileModel>(user));
        }

        [HttpGet]
        [Route("{id}/Full")]
        [Authorize(Roles = Roles.Admin)]
        public async Task<ActionResult<UserProfileDto>> GetFullUserProfileById(string id)
        {
            var user = await _userProfileService.GetUserProfileByIdWithDetails(id);
            if (user == null)
                return NotFound();
            return Ok(user);
        }

        [HttpGet]
        [Route("GetYourRoles")]
        [Authorize(Roles = Roles.User)]
        [ServiceFilter(typeof(IsAuthenticatedUserExistFilter))]
        public async Task<ActionResult<IEnumerable<string>>> GetRoles()
        {
            return Ok(await _userService.GetUserRoles(User.FindFirst(ClaimTypes.NameIdentifier).Value));
        }

        [HttpGet]
        [Route("GetYourProfile")]
        [Authorize(Roles = Roles.User)]
        [ServiceFilter(typeof(IsAuthenticatedUserExistFilter))]
        public async Task<ActionResult<UserProfileDto>> GetProfile()
        {
            var user = await _userProfileService.GetUserProfileByIdWithDetails(User.FindFirst(ClaimTypes.NameIdentifier)
                .Value);
            if (user == null)
            {
                return NotFound(new ResponseBuilder("User with such id was not found", HttpStatusCode.NotFound));
            }
            return Ok(user);
        }

        [HttpPost]
        [Route("AddToRole/{id}/{roleName}")]
        [Authorize(Roles = Roles.Admin)]
        public async Task<ActionResult<UserProfileDto>> AddUserToRole(string id, string roleName)
        {
            var user = await _userProfileService.GetUserProfileByIdWithDetails(id);
            if (user == null) 
                return NotFound();
            await _userService.AddToRole(user.Email, roleName);
            return NoContent();
        }

        [HttpPut]
        [Route("UpdateUser/{id}")]
        [Authorize(Roles = Roles.Admin)]
        public async Task<ActionResult<UserProfileDto>> UpdateUser([FromBody]UpdateUserModel userData, string id)
        {
            var user = await _userService.GetUserByIdWithDetails(id);
            if (user == null) 
                return BadRequest(
                new ResponseBuilder("User with passed id was not found", HttpStatusCode.BadRequest));
            var userToUpdate = _mapper.Map<UserDto>(userData);
            userToUpdate.Id = user.Id;
            await _userService.UpdateUser(userToUpdate);
            var userProfile = await _userProfileService.GetUserProfileByIdWithDetails(user.Id.ToString());
            return Ok(userProfile);
        }

        [HttpPut]
        [Route("UpdateUser")]
        [Authorize(Roles = Roles.User)]
        [ServiceFilter(typeof(IsAuthenticatedUserExistFilter))]
        public async Task<ActionResult<UserProfileDto>> UpdateUser([FromBody] UpdateUserModel userData)
        {
            var userToUpdate = _mapper.Map<UserDto>(userData);
            userToUpdate.Id = new Guid(User.FindFirst(ClaimTypes.NameIdentifier).Value);
            await _userService.UpdateUser(userToUpdate);
            var userProfile = await _userProfileService.GetUserProfileByIdWithDetails(userToUpdate.Id.ToString());
            return Ok(userProfile);
        }

        [HttpPut]
        [Route("ChangePassword")]
        [Authorize(Roles = Roles.User)]
        [ServiceFilter(typeof(IsAuthenticatedUserExistFilter))]
        public async Task<ActionResult> ChangeUserPassword([FromBody] ChangePasswordModel passwordModel)
        {
            await _userService.ChangeUserPassword(
                User.FindFirst(ClaimTypes.NameIdentifier).Value, 
                passwordModel.CurrentPassword,
                passwordModel.NewPassword);
            return NoContent();
        }

        [HttpDelete]
        [Route("DeleteUser/{id}")]
        [Authorize(Roles = Roles.Admin)]
        public async Task<ActionResult> DeleteUser(string id)
        {
            await _userService.DeleteUserById(id);
            return NoContent();
        }

        [HttpDelete]
        [Route("DeleteUser")]
        [Authorize(Roles = Roles.User)]
        [ServiceFilter(typeof(IsAuthenticatedUserExistFilter))]
        public async Task<ActionResult> DeleteUser()
        {
            await _userService.DeleteUserById(User.FindFirst(ClaimTypes.NameIdentifier).Value);
            return NoContent();
        }
    }
}
