﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Business.Constants;
using Business.Dtos;
using Business.Interfaces;
using Microsoft.AspNetCore.Authorization;
using WebApi.Filters;
using WebApi.Models;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme)]
    public class GenreController : ControllerBase
    {
        private readonly IGenreService _genreService;
        private readonly IMapper _mapper;

        public GenreController(IMapper mapper, IGenreService genreService)
        {
            _mapper = mapper;
            _genreService = genreService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<GenreDto>>> GetAllGenres()
        {
            return Ok(await _genreService.GetAllGenres());
        }

        [HttpGet]
        [Route("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<GenreDto>> GetGenreById(string id)
        {
            var genre = await _genreService.GetGenreById(id);
            if (genre == null)
            {
                return NotFound();
            }
            return Ok(genre);
        }

        [HttpPost]
        [Route("Add")]
        [Authorize(Roles = Roles.Admin)]
        [ServiceFilter(typeof(IsAuthenticatedUserExistFilter))]
        public async Task<ActionResult> AddGenre([FromBody] GenreModel genre)
        {
            var newGenreId = await _genreService.AddGenre(_mapper.Map<GenreDto>(genre));
            var newGenre = await _genreService.GetGenreById(newGenreId);
            return CreatedAtAction(nameof(GetGenreById), new {id = newGenreId}, newGenre);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        [Authorize(Roles = Roles.Admin)]
        public async Task<ActionResult> DeleteGenre(string id)
        {
            await _genreService.DeleteGenreById(id);
            return NoContent();
        }
    }
}
