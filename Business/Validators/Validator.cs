﻿using System;
using Business.Exceptions;

namespace Business.Validators
{
    /// <summary>
    /// Class to validate things
    /// </summary>
    public static class Validator
    {
        /// <summary>
        /// Validate if passed string is GUID
        /// </summary>
        /// <param name="guid">String that should be validated</param>
        /// <exception cref="CardFileException">Throws when passed string is not valid GUID</exception>
        public static void GuidCheck(this string guid)
        {
            if (!Guid.TryParse(guid, out _))
            {
                throw new CardFileException("Passed id was not valid");
            }
        }
    }
}
