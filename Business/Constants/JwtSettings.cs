﻿namespace Business.Constants
{
    /// <summary>
    /// Settings of JWT
    /// </summary>
    public static class JwtSettings
    {
        public const string ValidAudience = "http://localhost:4200/";
        public const string ValidIssuer = "https://localhost:44367";
        public const string Secret = "B8MmxLBXOiemD10jOqeUeG6PuakW0cXff97mgq2l0BIv5G2aCbvfLvJ4rqM1iTw";
    }
}
