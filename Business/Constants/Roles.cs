﻿namespace Business.Constants
{
    /// <summary>
    /// Static class that contains currently available roles
    /// </summary>
    public static class Roles
    {
        public const string Admin = "Admin";
        public const string User = "User";
    }
}
