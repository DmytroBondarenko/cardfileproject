﻿namespace Business.Helpers
{
    public static class CapitalizeString
    {
        /// <summary>
        /// Returns the input string lowercased with the first character converted to uppercase, or mutates any nulls passed into string.Empty
        /// </summary>
        public static string LowerCaseStringAndCapitalize(string s)
        {
            if (string.IsNullOrEmpty(s))
                return string.Empty;

            var str = s.ToLower();

            var a = str.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }
    }
}
