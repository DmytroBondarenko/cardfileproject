﻿using System;
using System.Runtime.Serialization;

namespace Business.Exceptions
{
    /// <summary>
    /// Exception that is thrown when something was not found
    /// </summary>
    [Serializable]
    public class NotFoundException : Exception
    {
        public NotFoundException() { }

        public NotFoundException(string message) : base(message) { }

        public NotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }
        protected NotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
