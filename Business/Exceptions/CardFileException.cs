﻿using System;
using System.Runtime.Serialization;

namespace Business.Exceptions
{
    /// <summary>
    /// General exception of the whole app. Throws when some of the passed parameters was not valid
    /// </summary>
    [Serializable]
    public class CardFileException : Exception
    {
        public CardFileException() { }

        public CardFileException(string message) : base(message) { }

        public CardFileException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CardFileException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
