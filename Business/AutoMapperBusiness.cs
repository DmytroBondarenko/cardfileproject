﻿using AutoMapper;
using System.Linq;
using Business.Dtos;
using Data.Entities;

namespace Business
{
    /// <summary>
    /// AutoMapper to map DTOs with database models
    /// </summary>
    public class AutoMapperBusiness : Profile
    {
        public AutoMapperBusiness()
        {
            CreateMap<string, GenreText>().ForMember(dest => dest.GenreId,
                m => m.MapFrom(src => src));

            CreateMap<TextDto, Text>()
                .ForMember(x => x.GenreTexts,
                    f => f.MapFrom(text => text.GenreIds));

            CreateMap<Text, TextDto>()
                .ForMember(x => x.GenreIds,
                    m => m.MapFrom(src => src.GenreTexts.Select(x => x.GenreId)));

            CreateMap<User, UserDto>()
                .ForMember(p => p.FirstName, c => c.MapFrom(src => src.UserProfile.FirstName))
                .ForMember(p => p.LastName, c => c.MapFrom(src => src.UserProfile.LastName))
                .ReverseMap();

            CreateMap<Genre, GenreDto>()
                .ReverseMap();

            CreateMap<UserProfile, UserProfileDto>()
                .ForMember(p => p.Email, c => c.MapFrom(src => src.AppUser.Email))
                .ForMember(p => p.TextIds, c => c.MapFrom(src => src.Texts.Select(x => x.Id)))
                .ForMember(p => p.UserName, c => c.MapFrom(src => src.AppUser.UserName))
                .ReverseMap();

            CreateMap<UserProfileDto, UserDto>().ReverseMap();
        }
    }
}
