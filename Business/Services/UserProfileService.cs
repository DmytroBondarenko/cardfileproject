﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Business.Dtos;
using Business.Exceptions;
using Business.Interfaces;
using Business.Validators;
using Data.Interfaces.BaseInterfaces;

namespace Business.Services
{
    /// <summary>
    /// Class to work with user profiles
    /// </summary>
    public class UserProfileService : IUserProfileService
    {
        /// <summary>
        /// Variable of IUnitOfWork type to access the database.
        /// </summary>
        private readonly IUnitOfWork _dataBase;

        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor for dependency injection.
        /// </summary>
        /// <param name="uow">Variable for database access.</param>
        /// <param name="mapper">AutoMapper parameter to map models</param>
        public UserProfileService(IUnitOfWork uow, IMapper mapper)
        {
            _dataBase = uow;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all user profiles with details
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> of user profiles</returns>
        public async Task<IEnumerable<UserProfileDto>> GetAllUserProfilesWithDetails()
        {
            return _mapper.Map<IEnumerable<UserProfileDto>>(await _dataBase.UserProfileRepository.GetAllWithDetailsAsync());
        }

        /// <summary>
        /// Get user profile with details
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns><see cref="UserProfileDto"/> with user profile info</returns>
        /// <exception cref="CardFileException">Throws when passed id is not valid</exception>
        public async Task<UserProfileDto> GetUserProfileByIdWithDetails(string id)
        {
            id.GuidCheck();
            return _mapper.Map<UserProfileDto>(await _dataBase.UserProfileRepository.GetByIdWithDetailsAsync(id));
        }

        /// <summary>
        /// Get user profile
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns><see cref="UserProfileDto"/> with user profile info</returns>
        /// <exception cref="CardFileException">Throws when passed id is not valid</exception>
        public async Task<UserProfileDto> GetUserProfileById(string id)
        {
            id.GuidCheck();
            return _mapper.Map<UserProfileDto>(await _dataBase.UserProfileRepository.GetByIdAsync(id));
        }
    }
}
