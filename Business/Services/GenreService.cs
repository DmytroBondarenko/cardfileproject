﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Business.Dtos;
using Business.Exceptions;
using Business.Helpers;
using Business.Interfaces;
using Business.Validators;
using Data.Entities;
using Data.Interfaces.BaseInterfaces;

namespace Business.Services
{
    /// <summary>
    /// Class to work with genres
    /// </summary>
    public class GenreService : IGenreService
    {
        /// <summary>
        /// Variable of IUnitOfWork type to access the database.
        /// </summary>
        private readonly IUnitOfWork _dataBase;

        /// <summary>
        /// AutoMapper parameter to map models
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor for dependency injection.
        /// </summary>
        /// <param name="uow">Variable for database access.</param>
        /// <param name="mapper">AutoMapper parameter to map models</param>
        public GenreService(IUnitOfWork uow, IMapper mapper)
        {
            _dataBase = uow;
            _mapper = mapper;
        }

        /// <summary>
        /// All genres
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> of genres</returns>
        public async Task<IEnumerable<GenreDto>> GetAllGenres()
        {
            return _mapper.Map<IEnumerable<GenreDto>>(await _dataBase.GenreRepository.GetAllAsync());
        }

        /// <summary>
        /// Get genre by id
        /// </summary>
        /// <param name="id">Genre id</param>
        /// <returns><see cref="GenreDto"/> with genre info</returns>
        /// <exception cref="CardFileException">Throws when passed user does not exist</exception>
        public async Task<GenreDto> GetGenreById(string id)
        {
            id.GuidCheck();
            return _mapper.Map<GenreDto>(await _dataBase.GenreRepository.GetByIdAsync(id));
        }

        /// <summary>
        /// Add new genre
        /// </summary>
        /// <param name="genre"><see cref="GenreDto"/> with genre info</param>
        /// <returns>New genre id</returns>
        /// <exception cref="CardFileException">Throws when passed genre was empty or already exist</exception>
        public async Task<string> AddGenre(GenreDto genre)
        {
            if (string.IsNullOrEmpty(genre.GenreName)) 
                throw new CardFileException("Passed genre name was empty");

            if (await GetGenreByName(CapitalizeString.LowerCaseStringAndCapitalize(genre.GenreName)) != null)
                throw new CardFileException("Genre with the same name already exist");

            var genreToAdd = _mapper.Map<Genre>(genre);
            genreToAdd.GenreName =
                CapitalizeString.LowerCaseStringAndCapitalize(genreToAdd.GenreName);
            await _dataBase.GenreRepository.AddAsync(genreToAdd);
            await _dataBase.SaveAsync();
            return genreToAdd.Id.ToString();
        }

        /// <summary>
        /// Delete genre by id
        /// </summary>
        /// <param name="id">Genre id</param>
        /// <exception cref="NotFoundException">Throws when genre with passed id does not exist</exception>
        /// <exception cref="CardFileException">Throws when passed id is not valid</exception>
        public async Task DeleteGenreById(string id)
        {
            id.GuidCheck();
            if (await GetGenreById(id) == null)
                throw new NotFoundException("Genre with passed id does not exist");

            _dataBase.GenreRepository.DeleteById(id);
            await _dataBase.SaveAsync();

            var texts = _mapper.Map<IEnumerable<TextDto>>(await _dataBase.TextRepository.GetAllWithDetailsAsync());
            foreach (var text in texts)
            {
                if (text.GenreIds.Count != 0) continue;
                _dataBase.TextRepository.DeleteById(text.Id.ToString());
                await _dataBase.SaveAsync();
            }
        }

        /// <summary>
        /// Get genre info by genre name
        /// </summary>
        /// <param name="name">Name of genre</param>
        /// <returns><see cref="GenreDto"/> with genre information</returns>
        public async Task<GenreDto> GetGenreByName(string name)
        {
            var genres = await GetAllGenres();
            return genres.FirstOrDefault(x => x.GenreName == name);
        }
    }
}
