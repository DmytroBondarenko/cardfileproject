﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Business.Dtos;
using Business.Exceptions;
using Business.Interfaces;
using Business.Validators;
using Data.Entities;
using Data.Interfaces.BaseInterfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Business.Services
{
    public class UserService : IUserService
    {
        /// <summary>
        /// Variable of IUnitOfWork type to access the database.
        /// </summary>
        private readonly IUnitOfWork _dataBase;

        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor for dependency injection.
        /// </summary>
        /// <param name="uow">Variable for database access.</param>
        /// <param name="mapper">AutoMapper parameter to map models</param>
        public UserService(IUnitOfWork uow, IMapper mapper)
        {
            _dataBase = uow;
            _mapper = mapper;
        }

        /// <summary>
        /// Creates new user
        /// </summary>
        /// <param name="user">New user</param>
        /// <param name="password">New user password</param>
        /// <exception cref="CardFileException">Throws when passed passed parameters was not valid,
        /// invalid password or user with same parameters already exist in database</exception>
        private async Task UserCreate(UserDto user, string password)
        {
            if (string.IsNullOrEmpty(password)
                || string.IsNullOrEmpty(user.FirstName) 
                || string.IsNullOrEmpty(user.LastName)
                || string.IsNullOrEmpty(user.UserName) 
                || string.IsNullOrEmpty(user.Email))
                throw new CardFileException("Some of the passed parameters was empty");

            user.FirstName = user.FirstName.Trim();
            user.LastName = user.LastName.Trim();

            if (password.Contains(" ") || user.Email.Contains(" ") || user.UserName.Contains(" "))
                throw new CardFileException("Email, password and username should not contain whitespaces");

            if (await _dataBase.UserRepository.GetByEmailAsync(user.Email) != null)
                throw new CardFileException("Such user with same email already exist");

            if (await GetUserByUserName(user.UserName) != null)
                throw new CardFileException("Such user with same username already exist");

            var result = await _dataBase.UserRepository.UserAddAsync(_mapper.Map<User>(user), password);
            if (!result.Succeeded)
                throw new CardFileException(
                    "Invalid password. Must be at least 6 characters long, have uppercase characters and special characters");

            await _dataBase.SaveAsync();
        }

        /// <summary>
        /// Adds new role to user
        /// </summary>
        /// <param name="email">Email of the user, which should be added to the new role</param>
        /// <param name="role">Role name</param>
        /// <returns><see cref="IdentityResult"/> with information about operation</returns>
        /// <exception cref="CardFileException">Throws when some of the parameters was empty or user already have passed role</exception>
        /// <exception cref="NotFoundException">Throws when user by passed email was not found or if passed role does not exist</exception>
        public async Task<IdentityResult> AddToRole(string email, string role)
        {
            if (string.IsNullOrEmpty(role)
                || string.IsNullOrEmpty(email))
                throw new CardFileException("Some of the passed parameters was empty");

            if (email.Contains(" "))
            {
                throw new CardFileException("Login should not contain whitespaces");
            }

            if (!await _dataBase.RoleRepository.RoleExistsAsync(role))
                throw new NotFoundException("Passed role does not exist");

            var userDetails = await _dataBase.UserRepository.GetByEmailAsync(email);
            if (userDetails == null)
                throw new NotFoundException("Passed user does not exist");

            var result =  await _dataBase.UserRepository.AddToRoleAsync(userDetails, role);
            if (!result.Succeeded)
                throw new CardFileException("User already has such role");

            await _dataBase.SaveAsync();
            return result;
        }

        /// <summary>
        /// Change user password
        /// </summary>
        /// <param name="userId">Id of the user, of which password will be changed</param>
        /// <param name="currentPassword">Current user password</param>
        /// <param name="newPassword">New password</param>
        /// <exception cref="CardFileException">Throws when some of the parameters was empty,
        /// when passed id is not valid or there was an error with changing password</exception>
        /// <exception cref="NotFoundException">Throws when user by passed id was not found</exception>
        public async Task ChangeUserPassword(string userId, string currentPassword, string newPassword)
        {
            userId.GuidCheck();
            if (string.IsNullOrEmpty(currentPassword) || string.IsNullOrEmpty(newPassword))
            {
                throw new CardFileException("Some of the passed parameters was empty");
            }

            if (newPassword.Contains(" "))
            {
                throw new CardFileException("Password should not contain whitespaces");
            }

            var user = await _dataBase.UserRepository.GetByIdAsync(userId);
            if (user == null)
            {
                throw new NotFoundException("User with passed id does not exist");
            }

            var result = await _dataBase.UserRepository.UserChangePasswordAsync(user, currentPassword, newPassword);
            if (!result.Succeeded)
            {
                throw new CardFileException("There was an error with changing password. " +
                                            "Maybe, your current password was not correct or new password was invalid " +
                                            "(Must be at least 6 characters long, have uppercase characters and special characters)");
            }
        }

        /// <summary>
        /// Create user and add to role
        /// </summary>
        /// <param name="user"><see cref="UserDto"/> with user information</param>
        /// <param name="password">Password of the user</param>
        /// <param name="role">Role to which will be added created user</param>
        /// <exception cref="NotFoundException">Throws when user by passed email was not found or if passed role does not exist</exception>
        /// <exception cref="CardFileException">Throws when some of the parameters was empty</exception>
        public async Task CreateUserAndAddToRole(UserDto user, string password, string role)
        {
            if (string.IsNullOrEmpty(role)
                || string.IsNullOrEmpty(user.FirstName)
                || string.IsNullOrEmpty(user.LastName)
                || string.IsNullOrEmpty(user.UserName)
                || string.IsNullOrEmpty(user.Email) 
                || string.IsNullOrEmpty(password))
                throw new CardFileException("Some of the passed parameters was empty");

            if (!await _dataBase.RoleRepository.RoleExistsAsync(role))
                throw new NotFoundException("Passed role does not exist");

            await UserCreate(user, password);
            await AddToRole(user.Email, role);
        }

        public async Task<UserDto> GetUserByUserName(string userName)
        {
            return _mapper.Map<UserDto>(await _dataBase.UserRepository.GetAll().FirstOrDefaultAsync(x => x.UserName == userName));
        }

        /// <summary>
        /// Get user by id
        /// </summary>
        /// <param name="id">Id of user</param>
        /// <returns><see cref="UserDto"/> with user info</returns>
        /// <exception cref="CardFileException">Throws when passed id is not valid</exception>
        public async Task<UserDto> GetUserById(string id)
        {
            id.GuidCheck();
            return _mapper.Map<UserDto>(await _dataBase.UserRepository.GetByIdAsync(id));
        }

        /// <summary>
        /// Get user by email
        /// </summary>
        /// <param name="email">Email of the user</param>
        /// <returns><see cref="UserDto"/> with user information</returns>
        public async Task<UserDto> GetUserByEmail(string email)
        {
            return _mapper.Map<UserDto>(await _dataBase.UserRepository.GetByEmailAsync(email));
        }

        /// <summary>
        /// Check user password
        /// </summary>
        /// <param name="email">Email of the user</param>
        /// <param name="password">Password of the user</param>
        /// <returns>True if password matches and false if not</returns>
        /// <exception cref="NotFoundException">Throws when user does not exist</exception>
        public async Task<bool> UserCheckPassword(string email, string password)
        {
            var user = await _dataBase.UserRepository.GetByEmailAsync(email);
            if (user == null)
                throw new NotFoundException("User with such email does not exist");

            return await _dataBase.UserRepository.UserCheckPasswordAsync(user, password);
        }

        /// <summary>
        /// Get user roles
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns><see cref="IEnumerable{T}"/> of user roles</returns>
        /// <exception cref="CardFileException">Throws when passed id is not valid</exception>
        /// <exception cref="NotFoundException">Throws when user does not exist</exception>
        public async Task<IEnumerable<string>> GetUserRoles(string id)
        {
            id.GuidCheck();
            var user = await _dataBase.UserRepository.GetByIdAsync(id);
            if (user == null)
                throw new NotFoundException("User with such email does not exist");

            return await _dataBase.UserRepository.UserGetRolesAsync(user);
        }

        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="user"><see cref="UserDto"/> with user information</param>
        /// <exception cref="CardFileException">Throws when passed passed parameters was not valid
        /// or user with same parameters already exist in database</exception>
        public async Task UpdateUser(UserDto user)
        {
            if (string.IsNullOrEmpty(user.FirstName)
                || string.IsNullOrEmpty(user.LastName)
                || string.IsNullOrEmpty(user.UserName)
                || string.IsNullOrEmpty(user.Email))
                throw new CardFileException("Some of the passed parameters was empty");

            user.FirstName = user.FirstName.Trim();
            user.LastName = user.LastName.Trim();

            if (user.Email.Contains(" ") || user.UserName.Contains(" ")) 
                throw new CardFileException("Email and username should not contain whitespaces");

            var searchResultByName = await GetUserByUserName(user.UserName);
            if (searchResultByName != null
                && string.CompareOrdinal(searchResultByName.Id.ToString(), user.Id.ToString()) != 0)
                throw new CardFileException("Such user with same username already exist");

            var searchResultByEmail = await _dataBase.UserRepository.GetByEmailAsync(user.Email);
            if (searchResultByEmail != null
                && string.CompareOrdinal(searchResultByEmail.Id.ToString(), user.Id.ToString()) != 0)
                throw new CardFileException("Such user with same email already exist");

            await _dataBase.UserRepository.UpdateAsync(_mapper.Map<User>(user));
            await _dataBase.SaveAsync();
        }

        /// <summary>
        /// Delete user by id
        /// </summary>
        /// <param name="id">User id</param>
        /// <exception cref="CardFileException">Throws when passed id is not valid</exception>
        /// <exception cref="NotFoundException">Throws when user with passed id does not exist</exception>
        public async Task DeleteUserById(string id)
        {
            id.GuidCheck();
            var user = await GetUserById(id);
            if (user == null)
                throw new NotFoundException("User with passed id does not exist");

            await _dataBase.UserRepository.DeleteByIdAsync(id);
            await _dataBase.SaveAsync();
        }

        /// <summary>
        /// Get user by id with details
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns><see cref="UserDto"/> with user information</returns>
        /// <exception cref="CardFileException">Throws when passed id is not valid</exception>
        public async Task<UserDto> GetUserByIdWithDetails(string id)
        {
            id.GuidCheck();
            return _mapper.Map<UserDto>(await _dataBase.UserRepository.GetByIdWithDetailsAsync(id));
        }
    }
}
