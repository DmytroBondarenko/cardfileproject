﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Business.Dtos;
using Business.Exceptions;
using Business.Helpers;
using Business.Interfaces;
using Business.Validators;
using Data.Entities;
using Data.Interfaces.BaseInterfaces;
using Microsoft.EntityFrameworkCore;

namespace Business.Services
{
    /// <summary>
    /// Class to work with the texts
    /// </summary>
    public class TextService : ITextService
    {
        /// <summary>
        /// Variable of IUnitOfWork type to access the database.
        /// </summary>
        private readonly IUnitOfWork _dataBase;

        /// <summary>
        /// AutoMapper parameter to map models
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// Genre service to work with genres
        /// </summary>
        private readonly IGenreService _genreService;

        /// <summary>
        /// Constructor for dependency injection.
        /// </summary>
        /// <param name="uow">Variable for database access.</param>
        /// <param name="mapper">AutoMapper parameter to map models</param>
        /// <param name="genreService">Genre service</param>
        public TextService(IUnitOfWork uow, 
            IMapper mapper, IGenreService genreService)
        {
            _dataBase = uow;
            _mapper = mapper;
            _genreService = genreService;
        }

        /// <summary>
        /// Add new text
        /// </summary>
        /// <param name="text"><see cref="TextDto"/> with text info</param>
        /// <returns>New genre id</returns>
        /// <exception cref="CardFileException">Throws when some of the passed parameters were empty,
        /// list with genres was empty or one of the passed genres does not exist</exception>
        public async Task<string> AddText(TextDto text)
        {
            if (string.IsNullOrEmpty(text.Title) 
                || string.IsNullOrEmpty(text.Body))
                throw new CardFileException("Some of the passed parameters were empty");

            if (text.GenreIds.Count == 0)
                throw new CardFileException("List of genres was empty");

            text.GenreIds = text.GenreIds.Distinct().ToList();
            foreach (var genre in text.GenreIds)
            {
                var searchResult = await _genreService.GetGenreById(genre);

                if (searchResult == null)
                    throw new CardFileException("One of the passed genres does not exist");
            }

            var textToAdd = _mapper.Map<Text>(text);
            await _dataBase.TextRepository.AddAsync(textToAdd);
            await _dataBase.SaveAsync();
            return textToAdd.Id.ToString();
        }

        /// <summary>
        /// Get text rating
        /// </summary>
        /// <param name="textId">Rating id</param>
        /// <returns>Text rating</returns>
        /// <exception cref="CardFileException">Throws when passed id is not valid</exception>
        public async Task<int> GetTextRating(string textId)
        {
            textId.GuidCheck();
            var ratings = await _dataBase.RatingRepository.GetAllAsync();
            var rating = ratings.Count(x => x.TextId.ToString() == textId);
            return rating;
        }

        /// <summary>
        /// Get all texts with details
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> of texts</returns>
        public async Task<IEnumerable<TextDto>> GetAllTextsWithDetails()
        {
            var texts = _mapper.Map<IEnumerable<TextDto>>(await _dataBase.TextRepository.GetAllWithDetailsAsync()).ToList();
            foreach (var text in texts)
            {
                var rating = await GetTextRating(text.Id.ToString());
                text.Rating = rating;
            }
            return texts;
        }

        /// <summary>
        /// Get text by id
        /// </summary>
        /// <param name="id">Id of text</param>
        /// <returns><see cref="TextDto"/> with text info</returns>
        /// <exception cref="CardFileException">Throws when passed id is not valid</exception>
        /// <exception cref="NotFoundException">Throws when text with passed id does not exist</exception>
        private async Task<TextDto> GetTextById(string id)
        {
            id.GuidCheck();
            var text = _mapper.Map<TextDto>(await _dataBase.TextRepository.GetByIdAsync(id));
            if (text == null)
                throw new NotFoundException("Text with passed id does not exist");

            text.Rating = await GetTextRating(text.Id.ToString());
            return text;
        }

        /// <summary>
        /// Get text by id with details
        /// </summary>
        /// <param name="id">Id of text</param>
        /// <returns><see cref="TextDto"/> with text info</returns>
        /// <exception cref="CardFileException">Throws when passed id is not valid</exception>
        /// <exception cref="NotFoundException">Throws when passed text with passed id does not exist</exception>
        public async Task<TextDto> GetTextByIdWithDetails(string id)
        {
            id.GuidCheck();
            var text = _mapper.Map<TextDto>(await _dataBase.TextRepository.GetByIdWithDetailsAsync(id));
            if (text == null)
                throw new NotFoundException("Text with passed id does not exist");

            text.Rating = await GetTextRating(text.Id.ToString());
            return text;
        }

        /// <summary>
        /// Delete text by id
        /// </summary>
        /// <param name="id">Text id</param>
        /// <exception cref="CardFileException">Throws when passed id is not valid</exception>
        /// <exception cref="NotFoundException">Throws when passed text with passed id does not exist</exception>
        public async Task DeleteTextById(string id)
        {
            id.GuidCheck();
            if (GetTextById(id) == null)
                throw new NotFoundException("Text with passed id does not exist");

            _dataBase.TextRepository.DeleteById(id);
            await _dataBase.SaveAsync();
        }

        /// <summary>
        /// Get all texts by genre name
        /// </summary>
        /// <param name="genreName">Genre name</param>
        /// <returns><see cref="IEnumerable{T}"/> of texts</returns>
        /// <exception cref="NotFoundException">Throws when passed genre was not found</exception>
        public async Task<IEnumerable<TextDto>> GetAllTextsByGenreName(string genreName)
        {
            var genre = await _genreService.GetGenreByName(CapitalizeString.LowerCaseStringAndCapitalize(genreName));
            if (genre == null)
                throw new NotFoundException("Passed genre does not exist");

            var text = await GetAllTextsWithDetails();
            return text.Where(x => x.GenreIds.Any(id => id == genre.Id.ToString()));
        }

        /// <summary>
        /// Get all texts by username
        /// </summary>
        /// <param name="userName">Username of user</param>
        /// <returns><see cref="IEnumerable{T}"/> of texts</returns>
        /// <exception cref="NotFoundException">Throws when user was not found</exception>
        public async Task<IEnumerable<TextDto>> GetAllTextsByUserName(string userName)
        {
            var user = await _dataBase.UserRepository.GetAll()
                .FirstOrDefaultAsync(x => x.UserName == userName);
            if (user == null)
                throw new NotFoundException("User was not found");

            var text = await GetAllTextsWithDetails();
            return text.Where(x => x.UserId.ToString() == user.Id.ToString());
        }

        /// <summary>
        /// Get all texts by passed user id
        /// </summary>
        /// <param name="id">Id of the user</param>
        /// <returns><see cref="IEnumerable{T}"/> of texts of certain user</returns>
        /// <exception cref="CardFileException">Throws when passed id is not valid</exception>
        public async Task<IEnumerable<TextDto>> GetAllTextsByUserId(string id)
        {
            id.GuidCheck();
            var text = await GetAllTextsWithDetails();
            return text.Where(x => x.UserId.ToString() == id);
        }

        /// <summary>
        /// Get all text sorted by rating in ascending order
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> of texts</returns>
        public async Task<IEnumerable<TextDto>> SortTextsByRatingAscending()
        {
            var texts = await GetAllTextsWithDetails();
            return _mapper.Map<IEnumerable<TextDto>>(texts.OrderBy(x => x.Rating));
        }

        /// <summary>
        /// Get all text sorted by rating in descending order
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> of texts</returns>
        public async Task<IEnumerable<TextDto>> SortTextsByRatingDescending()
        {
            var texts = await GetAllTextsWithDetails();
            return _mapper.Map<IEnumerable<TextDto>>(texts.OrderByDescending(x => x.Rating));
        }

        /// <summary>
        /// Rate text
        /// </summary>
        /// <param name="textId">Id of rated text</param>
        /// <param name="userId">User that is rating</param>
        /// <exception cref="CardFileException">Throws when some of the passed ids is not valid</exception>
        public async Task RateText(string textId, string userId)
        {
            textId.GuidCheck();

            userId.GuidCheck();

            var text = await GetTextById(textId);

            if (text == null)
                throw new NotFoundException("Text with passed id does not exist");

            var ratings = await _dataBase.RatingRepository.GetAllAsync();

            if (ratings.FirstOrDefault(x => x.TextId.ToString() == textId && x.UserId.ToString() == userId) != null)
                throw new CardFileException("You already rated this text");

            await _dataBase.RatingRepository.AddAsync(new Rating
            {
                TextId = new Guid(textId),
                UserId = new Guid(userId)
            });

            await _dataBase.SaveAsync();
        }
    }
}
