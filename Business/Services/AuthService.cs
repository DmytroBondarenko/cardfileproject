﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Business.Constants;
using Business.Dtos;
using Business.Exceptions;
using Business.Interfaces;
using Microsoft.IdentityModel.Tokens;

namespace Business.Services
{
    /// <summary>
    /// Class that represents login-register functions
    /// </summary>
    public class AuthService : IAuthService
    {
        /// <summary>
        /// Service to work with users
        /// </summary>
        private readonly IUserService _service;

        /// <summary>
        /// AutoMapper parameter to map models
        /// </summary>
        private readonly IMapper _mapper;


        /// <summary>
        /// Constructor for dependency injection.
        /// </summary>
        /// <param name="service"></param>
        /// <param name="mapper">AutoMapper parameter to map models</param>
        public AuthService(IUserService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        /// <summary>
        /// Method which will register new user
        /// </summary>
        /// <param name="user">User information</param>
        /// <param name="password">User password</param>
        /// <returns><see cref="UserProfileDto"/> as new user profile information</returns>
        public async Task<UserProfileDto> RegisterUser(UserDto user, string password)
        {
            await _service.CreateUserAndAddToRole(user, password, Roles.User);
            return _mapper.Map<UserProfileDto>(await _service.GetUserByEmail(user.Email));
        }

        /// <summary>
        /// Method which will login user and generate JWT of user
        /// </summary>
        /// <param name="user">User information</param>
        /// <returns><see cref="LoggedUserDto"/> with token and username</returns>
        /// <exception cref="NotFoundException">Throws when passed user was not found</exception>
        public async Task<LoggedUserDto> LoginUser(UserDto user)
        {
            var authSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(JwtSettings.Secret));
            var searchedUser = await _service.GetUserByEmail(user.Email);
            if (searchedUser == null)
                throw new NotFoundException("Passed user was not found");

            var userRoles = await _service.GetUserRoles(searchedUser.Id.ToString());
            var authClaims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
            };

            authClaims.AddRange(userRoles.Select(userRole => new Claim(ClaimTypes.Role, userRole)));

            var claimsIdentity = new ClaimsIdentity(authClaims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);

            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                issuer: JwtSettings.ValidIssuer,
                audience: JwtSettings.ValidAudience,
                notBefore: now,
                claims: claimsIdentity.Claims,
                expires: now.AddDays(1),
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256Signature));

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return new LoggedUserDto(encodedJwt, searchedUser.Id.ToString());
        }
    }
}
