﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Business.Dtos;

namespace Business.Interfaces
{
    /// <summary>
    /// The interface that determines the general methods to work with user profiles
    /// </summary>
    public interface IUserProfileService
    {
        /// <summary>
        /// Get all user profiles with details
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> of user profiles</returns>
        Task<IEnumerable<UserProfileDto>> GetAllUserProfilesWithDetails();

        /// <summary>
        /// Get user profile with details
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns><see cref="UserProfileDto"/> with user profile info</returns>
        Task<UserProfileDto> GetUserProfileByIdWithDetails(string id);

        /// <summary>
        /// Get user profile
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns><see cref="UserProfileDto"/> with user profile info</returns>
        Task<UserProfileDto> GetUserProfileById(string id);
    }
}
