﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Business.Dtos;

namespace Business.Interfaces
{
    /// <summary>
    /// The interface that determines the general methods to work with texts
    /// </summary>
    public interface ITextService
    {
        /// <summary>
        /// Add new text
        /// </summary>
        /// <param name="text"><see cref="TextDto"/> with text info</param>
        /// <returns>New genre id</returns>
        Task<string> AddText(TextDto text);

        /// <summary>
        /// Get all texts with details
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> of texts</returns>
        Task<IEnumerable<TextDto>> GetAllTextsWithDetails();

        /// <summary>
        /// Get text by id with details
        /// </summary>
        /// <param name="id">Id of text</param>
        /// <returns><see cref="TextDto"/> with text info</returns>
        Task<TextDto> GetTextByIdWithDetails(string id);

        /// <summary>
        /// Get all texts by username
        /// </summary>
        /// <param name="userName">Username of user</param>
        /// <returns><see cref="IEnumerable{T}"/> of texts</returns>
        Task<IEnumerable<TextDto>> GetAllTextsByUserName(string userName);

        /// <summary>
        /// Get all texts by passed user id
        /// </summary>
        /// <param name="id">Id of the user</param>
        /// <returns><see cref="IEnumerable{T}"/> of texts of certain user</returns>
        Task<IEnumerable<TextDto>> GetAllTextsByUserId(string id);

        /// <summary>
        /// Delete text
        /// </summary>
        /// <param name="id">Id of the text</param>
        Task DeleteTextById(string id);

        /// <summary>
        /// Get all text sorted by rating in ascending order
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> of texts</returns>
        Task<IEnumerable<TextDto>> SortTextsByRatingAscending();

        /// <summary>
        /// Get all text sorted by rating in descending order
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> of texts</returns>
        Task<IEnumerable<TextDto>> SortTextsByRatingDescending();

        /// <summary>
        /// Get all texts by genre name
        /// </summary>
        /// <param name="genreName">Genre name</param>
        /// <returns><see cref="IEnumerable{T}"/> of texts</returns>
        Task<IEnumerable<TextDto>> GetAllTextsByGenreName(string genreName);

        /// <summary>
        /// Rate text
        /// </summary>
        /// <param name="textId">Id of rated text</param>
        /// <param name="userId">User that is rating</param>
        Task RateText(string textId, string userId);

    }
}
