﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Business.Dtos;
using Microsoft.AspNetCore.Identity;

namespace Business.Interfaces
{
    /// <summary>
    /// The interface that determines the general methods to work with user
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        /// Adds new role to user
        /// </summary>
        /// <param name="email">Email of the user</param>
        /// <param name="role">Role to which will be added user</param>
        /// <returns><see cref="IdentityResult"/> with the result of adding</returns>
        Task<IdentityResult> AddToRole(string email, string role);

        /// <summary>
        /// Create user and add to role
        /// </summary>
        /// <param name="user"><see cref="UserDto"/> with user information</param>
        /// <param name="password">Password of the user</param>
        /// <param name="role">Role to which will be added created user</param>
        Task CreateUserAndAddToRole(UserDto user, string password, string role);

        /// <summary>
        /// Get user by email
        /// </summary>
        /// <param name="email">Email of the user</param>
        /// <returns><see cref="UserDto"/> with user information</returns>
        Task<UserDto> GetUserByEmail(string email);

        /// <summary>
        /// Check user password
        /// </summary>
        /// <param name="email">Email of the user</param>
        /// <param name="password">Password of the user</param>
        /// <returns>True if password matches and false if not</returns>
        Task<bool> UserCheckPassword(string email, string password);

        /// <summary>
        /// Get user roles
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns><see cref="IEnumerable{T}"/> of user roles</returns>
        Task<IEnumerable<string>> GetUserRoles(string id);

        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="user"><see cref="UserDto"/> with user information</param>
        Task UpdateUser(UserDto user);

        /// <summary>
        /// Delete user by id
        /// </summary>
        /// <param name="id">User id</param>
        Task DeleteUserById(string id);

        /// <summary>
        /// Get user by id with details
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns><see cref="UserDto"/> with user information</returns>
        Task<UserDto> GetUserByIdWithDetails(string id);

        /// <summary>
        /// Change user password
        /// </summary>
        /// <param name="userId">Id of the user, of which password will be changed</param>
        /// <param name="currentPassword">Current user password</param>
        /// <param name="newPassword">New password</param>
        Task ChangeUserPassword(string userId, string currentPassword, string newPassword);

    }
}
