﻿using System.Threading.Tasks;
using Business.Dtos;

namespace Business.Interfaces
{
    /// <summary>
    /// Interface which contains methods to login and register
    /// </summary>
    public interface IAuthService
    {
        /// <summary>
        /// Method which will register new user
        /// </summary>
        /// <param name="user">User information</param>
        /// <param name="password">User password</param>
        /// <returns><see cref="UserProfileDto"/> as new user profile information</returns>
        Task<UserProfileDto> RegisterUser(UserDto user, string password);

        /// <summary>
        /// Method which will login user and generate JWT of user
        /// </summary>
        /// <param name="user">User information</param>
        /// <returns><see cref="LoggedUserDto"/> with token and username</returns>
        Task<LoggedUserDto> LoginUser(UserDto user);
    }
}
