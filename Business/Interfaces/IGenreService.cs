﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Business.Dtos;

namespace Business.Interfaces
{
    /// <summary>
    /// The interface that determines the general methods to work with genres
    /// </summary>
    public interface IGenreService
    {
        /// <summary>
        /// All genres
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> of genres</returns>
        Task<IEnumerable<GenreDto>> GetAllGenres();

        /// <summary>
        /// Get genre by id
        /// </summary>
        /// <param name="id">Genre id</param>
        /// <returns><see cref="GenreDto"/> with genre info</returns>
        Task<GenreDto> GetGenreById(string id);

        /// <summary>
        /// Add new genre
        /// </summary>
        /// <param name="genre"><see cref="GenreDto"/> with genre info</param>
        /// <returns>New genre id</returns>
        Task<string> AddGenre(GenreDto genre);

        /// <summary>
        /// Delete genre by id
        /// </summary>
        /// <param name="id">Genre id</param>
        Task DeleteGenreById(string id);

        /// <summary>
        /// Get genre info by genre name
        /// </summary>
        /// <param name="name">Name of genre</param>
        /// <returns><see cref="GenreDto"/> with genre information</returns>
        Task<GenreDto> GetGenreByName(string name);
    }
}
