﻿using System;
using System.Collections.Generic;

namespace Business.Dtos
{
    /// <summary>
    /// Class that represents user profile
    /// </summary>
    public class UserProfileDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public virtual ICollection<Guid> TextIds { get; set; }
    }
}
