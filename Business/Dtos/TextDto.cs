﻿using System;
using System.Collections.Generic;

namespace Business.Dtos
{
    /// <summary>
    /// Class that represents text
    /// </summary>
    public class TextDto
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public int Rating { get; set; }
        public DateTime PublishDate { get; set; }
        public ICollection<string> GenreIds { get; set; }
    }
}
