﻿using System;
using Data.Entities;

namespace Business.Dtos
{
    /// <summary>
    /// Class that represents all needed user information
    /// </summary>
    public class UserDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public UserProfile UserProfile { get; set; }
    }
}
