﻿namespace Business.Dtos
{
    /// <summary>
    /// Class that represents logged user information
    /// </summary>
    public class LoggedUserDto
    {
        public string Token { get; }
        public string UserId { get; }

        public LoggedUserDto(string token, string userId)
        {
            Token = token;
            UserId = userId;
        }
    }
}
