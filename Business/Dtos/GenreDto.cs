﻿using System;

namespace Business.Dtos
{
    /// <summary>
    /// Class that represents genre
    /// </summary>
    public class GenreDto
    {
        public Guid Id { get; set; }
        public string GenreName { get; set; }
    }
}
