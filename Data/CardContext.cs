﻿using System;
using Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Data
{
    /// <summary>
    /// Context of the database
    /// </summary>
    public class CardContext : IdentityDbContext<User, Role, Guid>
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="options">Context options</param>
        public CardContext(DbContextOptions<CardContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            var role1 = new Role { Id = Guid.NewGuid(), Name = "Admin", NormalizedName = "ADMIN" };
            var role2 = new Role { Id = Guid.NewGuid(), Name = "User", NormalizedName = "USER" };

            builder.Entity<Role>().HasData(role1, role2);

            var adminAuthData = new User
            {
                Id = Guid.NewGuid(),
                Email = "admin@gmail.com",
                NormalizedEmail = "ADMIN@GMAIL.COM",
                UserName = "Admin",
                NormalizedUserName = "ADMIN"
            };

            var adminProfile = new UserProfile
            {
                Id = adminAuthData.Id,
                FirstName = "Admin",
                LastName = "Admin"
            };

            var hasher = new PasswordHasher<User>();
            var adminPassword = hasher.HashPassword(adminAuthData, "Admin123!");

            adminAuthData.PasswordHash = adminPassword;

            builder.Entity<UserProfile>().HasData(adminProfile);
            builder.Entity<User>().HasData(adminAuthData);
            builder.Entity<IdentityUserRole<Guid>>().HasData(
                new IdentityUserRole<Guid> { RoleId = role1.Id, UserId = adminProfile.Id },
                new IdentityUserRole<Guid> { RoleId = role2.Id, UserId = adminProfile.Id });

            var genres = new[]
            {
               new Genre { Id = Guid.NewGuid(), GenreName = "Detective" },
               new Genre { Id = Guid.NewGuid(), GenreName = "Horror" },
               new Genre { Id = Guid.NewGuid(), GenreName = "Comedy" },
               new Genre { Id = Guid.NewGuid(), GenreName = "Fantasy" },
               new Genre { Id = Guid.NewGuid(), GenreName = "Adventure" },
               new Genre { Id = Guid.NewGuid(), GenreName = "Romance" },
               new Genre { Id = Guid.NewGuid(), GenreName = "Westerns" },
               new Genre { Id = Guid.NewGuid(), GenreName = "History" },
               new Genre { Id = Guid.NewGuid(), GenreName = "Children’s" },
            };

            builder.Entity<Genre>().HasData(genres);
            builder.Entity<GenreText>().HasKey(sc => new { sc.GenreId, sc.TextId });
        }

        public DbSet<GenreText> GenreTexts { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Text> Texts { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Rating> Ratings { get; set; }
    }
}
