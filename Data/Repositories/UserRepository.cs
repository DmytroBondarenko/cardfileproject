﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Entities;
using Data.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories
{
    /// <summary>
    /// Class to work with users in database
    /// </summary>
    /// <inheritdoc cref="IUserRepository"/>
    public class UserRepository : IUserRepository
    {
        /// <summary>
        /// APIs for managing users
        /// </summary>
        private readonly UserManager<User> _userManager;

        /// <summary>
        /// Standard constructor.
        /// </summary>
        /// <param name="userManager">User manager</param>
        public UserRepository(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        public IQueryable<User> GetAll()
        {
            return _userManager.Users;
        }

        public async Task<User> GetByIdAsync(string id)
        {
            return await _userManager.FindByIdAsync(id);
        }

        public async Task<User> GetByEmailAsync(string email)
        {
            return await _userManager.FindByEmailAsync(email);
        }

        public async Task<bool> UserCheckPasswordAsync(User entity, string password)
        {
            return await _userManager.CheckPasswordAsync(entity, password);
        }

        public async Task<IdentityResult> UserAddAsync(User entity, string password)
        {
            return await _userManager.CreateAsync(entity, password);
        }

        public async Task<IdentityResult> AddToRoleAsync(User user, string role)
        {
            return await _userManager.AddToRoleAsync(user, role);
        }

        public async Task<IEnumerable<string>> UserGetRolesAsync(User user)
        {
            return await _userManager.GetRolesAsync(user);
        }

        public async Task<IdentityResult> UserChangePasswordAsync(User user, string currentPassword, string newPassword)
        {
            return await _userManager.ChangePasswordAsync(user, currentPassword, newPassword);
        }

        public async Task UpdateAsync(User entity)
        {
            var user = await GetByIdWithDetailsAsync(entity.Id.ToString());
            if (user != null)
            {
                user.Email = entity.Email;
                user.NormalizedEmail = entity.Email.ToUpper();
                user.UserName = entity.UserName;
                user.NormalizedUserName = entity.UserName.ToUpper();
                user.UserProfile.FirstName = entity.UserProfile.FirstName;
                user.UserProfile.LastName = entity.UserProfile.LastName;
            }
        }

        public async Task DeleteByIdAsync(string id)
        {
            var user = await GetByIdAsync(id);
            if (user != null)
            {
                await _userManager.DeleteAsync(user);
            }
        }

        public async Task<User> GetByIdWithDetailsAsync(string id)
        {
            return await _userManager.Users.Include(x => x.UserProfile).FirstOrDefaultAsync(x => x.Id.ToString() == id);
        }
    }
}
