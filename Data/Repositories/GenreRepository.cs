﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Data.Entities;
using Data.Interfaces.BaseInterfaces;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories
{
    /// <summary>
    /// Class to work with genre entity
    /// </summary>
    /// <inheritdoc cref="IRepository{Genre}"/>
    public class GenreRepository : IRepository<Genre>
    {
        /// <summary>
        /// Database context
        /// </summary>
        private readonly CardContext _db;

        /// <summary>
        /// Standard constructor.
        /// </summary>
        /// <param name="dataBase">Database context</param>
        public GenreRepository(CardContext dataBase)
        {
            _db = dataBase;
        }

        public async Task<IQueryable<Genre>> GetAllAsync()
        {
            var genres = await _db.Genres.ToListAsync();
            return genres.AsQueryable();
        }

        public async Task<Genre> GetByIdAsync(string id)
        {
            return await _db.Genres.FindAsync(new Guid(id));
        }

        public async Task AddAsync(Genre entity)
        {
            await _db.Genres.AddAsync(entity);
        }

        public void Update(Genre entity)
        {
            _db.Entry(entity).State = EntityState.Modified;
        }

        public void DeleteById(string id)
        {
            var reader = _db.Genres.Find(new Guid(id));
            if (reader != null)
            {
                _db.Genres.Remove(reader);
            }
        }
    }
}
