﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories
{
    /// <summary>
    /// Class to work texts in database
    /// </summary>
    /// <inheritdoc cref="ITextRepository"/>
    public class TextRepository : ITextRepository
    {
        /// <summary>
        /// Database context
        /// </summary>
        private readonly CardContext _db;

        /// <summary>
        /// Standard constructor.
        /// </summary>
        /// <param name="dataBase">Database context</param>
        public TextRepository(CardContext dataBase)
        {
            _db = dataBase;
        }

        public async Task<IQueryable<Text>> GetAllAsync()
        {
            var texts = await _db.Texts.ToListAsync();
            return texts.AsQueryable();
        }

        public async Task<Text> GetByIdAsync(string id)
        {
            return await _db.Texts.FindAsync(new Guid(id));
        }

        public async Task AddAsync(Text entity)
        {
            await _db.Texts.AddAsync(entity);
        }

        public void Update(Text entity)
        {
            _db.Entry(entity).State = EntityState.Modified;
        }

        public void DeleteById(string id)
        {
            var reader = _db.Texts.Find(new Guid(id));
            if (reader != null)
            {
                _db.Texts.Remove(reader);
            }
        }

        public async Task<IQueryable<Text>> GetAllWithDetailsAsync()
        {
            var texts = await _db.Texts.Include(x => x.GenreTexts).ToListAsync();
            return texts.AsQueryable();
        }

        public async Task<Text> GetByIdWithDetailsAsync(string id)
        {
            return await _db.Texts.Include(x => x.GenreTexts).FirstOrDefaultAsync(x => x.Id.ToString() == id);
        }
    }
}
