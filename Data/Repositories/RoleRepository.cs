﻿using System.Threading.Tasks;
using Data.Entities;
using Data.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace Data.Repositories
{
    /// <summary>
    /// Class to work with role repository
    /// </summary>
    /// <inheritdoc cref="IRoleRepository"/>
    public class RoleRepository : IRoleRepository
    {
        /// <summary>
        /// APIs for managing roles
        /// </summary>
        private readonly RoleManager<Role> _roleManager;

        /// <summary>
        /// Standard constructor.
        /// </summary>
        /// <param name="roleManager">Role manager</param>
        public RoleRepository(RoleManager<Role> roleManager)
        {
            _roleManager = roleManager;
        }

        public async Task<bool> RoleExistsAsync(string roleName)
        {
            return await _roleManager.RoleExistsAsync(roleName);
        }
    }
}
