﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories
{
    /// <summary>
    /// Class to work with user profiles in database
    /// </summary>
    /// <inheritdoc cref="IUserProfileRepository"/>
    public class UserProfileRepository : IUserProfileRepository
    {
        /// <summary>
        /// Database context
        /// </summary>
        private readonly CardContext _db;

        /// <summary>
        /// Standard constructor.
        /// </summary>
        /// <param name="dataBase">Database context</param>
        public UserProfileRepository(CardContext dataBase)
        {
            _db = dataBase;
        }


        public async Task<IQueryable<UserProfile>> GetAllWithDetailsAsync()
        {
            var profiles = await _db.UserProfiles
                .Include(x => x.Texts)
                .Include(x => x.AppUser).ToListAsync();
            return profiles.AsQueryable();
        }

        public async Task<UserProfile> GetByIdAsync(string id)
        {
            return await _db.UserProfiles.FindAsync(new Guid(id));
        }

        public async Task<UserProfile> GetByIdWithDetailsAsync(string id)
        {
            return await _db.UserProfiles
                .Include(x => x.Texts)
                .Include(x => x.AppUser)
                .FirstOrDefaultAsync(x => x.Id.ToString() == id);
        }
    }
}
