﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Data.Entities;
using Data.Interfaces.BaseInterfaces;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories
{
    /// <summary>
    /// Class to work with rating entity
    /// </summary>
    /// <inheritdoc cref="IRepository{Genre}"/>
    public class RatingRepository : IRepository<Rating>
    {
        /// <summary>
        /// Database context
        /// </summary>
        private readonly CardContext _db;

        /// <summary>
        /// Standard constructor.
        /// </summary>
        /// <param name="dataBase">Database context</param>
        public RatingRepository(CardContext dataBase)
        {
            _db = dataBase;
        }

        public async Task AddAsync(Rating entity)
        {
            await _db.Ratings.AddAsync(entity);
        }

        public void DeleteById(string id)
        {
            var rating = _db.Ratings.Find(new Guid(id));
            if (rating != null)
            {
                _db.Ratings.Remove(rating);
            }
        }

        public async Task<IQueryable<Rating>> GetAllAsync()
        {
            var ratings = await _db.Ratings.ToListAsync();
            return ratings.AsQueryable();
        }

        public async Task<Rating> GetByIdAsync(string id)
        {
            return await _db.Ratings.FindAsync(new Guid(id));
        }

        public void Update(Rating entity)
        {
            _db.Entry(entity).State = EntityState.Modified;
        }
    }
}
