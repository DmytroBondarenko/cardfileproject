﻿using System.Threading.Tasks;
using Data.Entities;
using Data.Interfaces;
using Data.Interfaces.BaseInterfaces;

namespace Data
{
    /// <summary>
    /// Class to work with all the repositories
    /// </summary>
    /// <inheritdoc cref="IUnitOfWork"/>
    public class UnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// Database context
        /// </summary>
        private readonly CardContext _db;

        /// <summary>
        /// Standard constructor
        /// </summary>
        /// <param name="db">Database context</param>
        /// <param name="textRepository">Text repository</param>
        /// <param name="genreRepository">Genre repository</param>
        /// <param name="userProfileRepository">User profile repository</param>
        /// <param name="roleRepository">Role repository</param>
        /// <param name="userRepository">User repository</param>
        /// <param name="ratingRepository">Rating repository</param>
        public UnitOfWork(CardContext db,
            ITextRepository textRepository, IRepository<Genre> genreRepository, 
            IUserProfileRepository userProfileRepository, IRoleRepository roleRepository, 
            IUserRepository userRepository, IRepository<Rating> ratingRepository)
        {
            _db = db;
            TextRepository = textRepository;
            GenreRepository = genreRepository;
            RoleRepository = roleRepository;
            UserProfileRepository = userProfileRepository;
            UserRepository = userRepository;
            RatingRepository = ratingRepository;
        }

        public ITextRepository TextRepository { get; }
        public IRepository<Genre> GenreRepository { get; }
        public IRepository<Rating> RatingRepository { get; }
        public IUserRepository UserRepository { get; }
        public IUserProfileRepository UserProfileRepository { get; }
        public IRoleRepository RoleRepository { get; }

        public async Task<int> SaveAsync()
        {
            return await _db.SaveChangesAsync();
        }
    }
}
