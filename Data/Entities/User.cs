﻿using System;
using Microsoft.AspNetCore.Identity;

namespace Data.Entities
{
    /// <summary>
    /// Class that represents user in database
    /// </summary>
    public class User : IdentityUser<Guid>
    {
        public UserProfile UserProfile { get; set; }
    }
}
