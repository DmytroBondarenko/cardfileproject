﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Entities
{
    /// <summary>
    /// Class that represents user profile in database
    /// </summary>
    public class UserProfile
    {
        [ForeignKey("AppUser")]
        public Guid Id { get; set; }

        [Required]
        [StringLength(100)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        public string LastName { get; set; }
        public virtual User AppUser { get; set; }
        public virtual ICollection<Text> Texts { get; set; }

    }
}
