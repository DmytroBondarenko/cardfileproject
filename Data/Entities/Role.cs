﻿using System;
using Microsoft.AspNetCore.Identity;

namespace Data.Entities
{
    /// <summary>
    /// Class that represents role in database
    /// </summary>
    public class Role : IdentityRole<Guid>
    {
    }
}
