﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Entities
{
    /// <summary>
    /// Class that represents text in database
    /// </summary>
    [Table("Text")]
    public class Text : BaseEntity
    {
        /// <summary>
        /// Text title
        /// </summary>
        [Required]
        [StringLength(100, MinimumLength = 50)]
        public string Title { get; set; }

        /// <summary>
        /// Text body
        /// </summary>
        [Required]
        [StringLength(1500, MinimumLength = 50)]
        public string Body { get; set; }

        /// <summary>
        /// Date when text was added
        /// </summary>
        [Required]
        public DateTime PublishDate { get; set; }

        [ForeignKey("User")]
        public Guid UserId { get; set; }

        public virtual UserProfile User { get; set; }

        /// <summary>
        /// Genres of Text
        /// </summary>
        public virtual ICollection<GenreText> GenreTexts { get; set; }
    }
}
