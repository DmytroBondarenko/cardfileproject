﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Entities
{
    /// <summary>
    /// Many to many relationship between genre and text
    /// </summary>
    public class GenreText
    {
        [ForeignKey("GenreId")]
        public Guid GenreId { get; set; }
        
        public virtual Genre Genre { get; set; }


        [ForeignKey("Text")]
        public Guid TextId { get; set; }
        
        public virtual Text Text { get; set; }
    }
}
