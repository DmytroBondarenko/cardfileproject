﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Entities
{
    /// <summary>
    /// Class that represent rating of the text in database
    /// </summary>
    [Table("Rating")]
    public class Rating : BaseEntity
    {
        public Guid UserId { get; set; }

        [ForeignKey("Text")]
        public Guid TextId { get; set; }
        public virtual Text Text { get; set; }
    }
}
