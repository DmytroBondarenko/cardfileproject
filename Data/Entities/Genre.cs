﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Entities
{
    /// <summary>
    /// Class that represents genre in database
    /// </summary>
    [Table("Genre")]
    public class Genre : BaseEntity
    {
        /// <summary>
        /// Text Genre
        /// </summary>
        [Required]
        [StringLength(100)]
        public string GenreName { get; set; }

        /// <summary>
        /// Many to many relationship with texts
        /// </summary>
        public virtual ICollection<GenreText> GenreTexts { get; set; }

    }
}
