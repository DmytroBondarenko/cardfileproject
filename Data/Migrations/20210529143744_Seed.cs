﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class Seed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("b41a7041-f3ec-4e45-ad1b-c34cddfe0d7b"), "67576d19-1c1e-41da-bc89-6f4c8a74a52a", "Admin", "ADMIN" },
                    { new Guid("40213456-366f-413c-9605-a6a503074b57"), "d709de50-6297-40b1-92ee-8ddc73e541f8", "User", "USER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { new Guid("f04e71a8-c47a-4c60-818f-32971b58cc1b"), 0, "f310758f-b0e6-436d-83bd-6507b9537ca5", "admin@gmail.com", false, false, null, "ADMIN@GMAIL.COM", "ADMIN", "AQAAAAEAACcQAAAAELmg1tQNdv+UR7gj6SkN4FP3SaNqDdZfNOQNQcc3AB8rCFxBls+4k9so6S1K2HDymA==", null, false, null, false, "Admin" });

            migrationBuilder.InsertData(
                table: "Genre",
                columns: new[] { "Id", "GenreName" },
                values: new object[,]
                {
                    { new Guid("988051fe-5e16-4cfe-9aff-76424a60d9be"), "Detective" },
                    { new Guid("de6dc5bb-23c4-41ac-857d-f5ea70d4c232"), "Horror" },
                    { new Guid("16a87d30-1df7-4f80-9e07-4e6be4733df5"), "Comedy" },
                    { new Guid("63da0fbc-532d-43e7-81da-0c8b6b7d362b"), "Fantasy" },
                    { new Guid("70b1c742-b4c2-4538-8a63-d4aefad0a512"), "Adventure" },
                    { new Guid("1cf3b02a-b4f6-4629-8127-4a22822d79aa"), "Romance" },
                    { new Guid("3c254410-fef2-4675-a953-50da7d1c59e1"), "Westerns" },
                    { new Guid("d096ee23-c4b5-4b64-a3c2-61754e26668a"), "History" },
                    { new Guid("13de33a7-631f-42dd-86bb-d08e9465d027"), "Children’s" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { new Guid("b41a7041-f3ec-4e45-ad1b-c34cddfe0d7b"), new Guid("f04e71a8-c47a-4c60-818f-32971b58cc1b") });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { new Guid("40213456-366f-413c-9605-a6a503074b57"), new Guid("f04e71a8-c47a-4c60-818f-32971b58cc1b") });

            migrationBuilder.InsertData(
                table: "UserProfiles",
                columns: new[] { "Id", "FirstName", "LastName" },
                values: new object[] { new Guid("f04e71a8-c47a-4c60-818f-32971b58cc1b"), "Admin", "Admin" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("40213456-366f-413c-9605-a6a503074b57"), new Guid("f04e71a8-c47a-4c60-818f-32971b58cc1b") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("b41a7041-f3ec-4e45-ad1b-c34cddfe0d7b"), new Guid("f04e71a8-c47a-4c60-818f-32971b58cc1b") });

            migrationBuilder.DeleteData(
                table: "Genre",
                keyColumn: "Id",
                keyValue: new Guid("13de33a7-631f-42dd-86bb-d08e9465d027"));

            migrationBuilder.DeleteData(
                table: "Genre",
                keyColumn: "Id",
                keyValue: new Guid("16a87d30-1df7-4f80-9e07-4e6be4733df5"));

            migrationBuilder.DeleteData(
                table: "Genre",
                keyColumn: "Id",
                keyValue: new Guid("1cf3b02a-b4f6-4629-8127-4a22822d79aa"));

            migrationBuilder.DeleteData(
                table: "Genre",
                keyColumn: "Id",
                keyValue: new Guid("3c254410-fef2-4675-a953-50da7d1c59e1"));

            migrationBuilder.DeleteData(
                table: "Genre",
                keyColumn: "Id",
                keyValue: new Guid("63da0fbc-532d-43e7-81da-0c8b6b7d362b"));

            migrationBuilder.DeleteData(
                table: "Genre",
                keyColumn: "Id",
                keyValue: new Guid("70b1c742-b4c2-4538-8a63-d4aefad0a512"));

            migrationBuilder.DeleteData(
                table: "Genre",
                keyColumn: "Id",
                keyValue: new Guid("988051fe-5e16-4cfe-9aff-76424a60d9be"));

            migrationBuilder.DeleteData(
                table: "Genre",
                keyColumn: "Id",
                keyValue: new Guid("d096ee23-c4b5-4b64-a3c2-61754e26668a"));

            migrationBuilder.DeleteData(
                table: "Genre",
                keyColumn: "Id",
                keyValue: new Guid("de6dc5bb-23c4-41ac-857d-f5ea70d4c232"));

            migrationBuilder.DeleteData(
                table: "UserProfiles",
                keyColumn: "Id",
                keyValue: new Guid("f04e71a8-c47a-4c60-818f-32971b58cc1b"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("40213456-366f-413c-9605-a6a503074b57"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("b41a7041-f3ec-4e45-ad1b-c34cddfe0d7b"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("f04e71a8-c47a-4c60-818f-32971b58cc1b"));
        }
    }
}
