﻿using System.Threading.Tasks;
using Data.Entities;
using Data.Interfaces.BaseInterfaces;

namespace Data.Interfaces
{
    /// <summary>
    /// Interface of user profile repository
    /// </summary>
    public interface IUserProfileRepository : IRepositoryExpanded<UserProfile>
    {
        /// <summary>
        /// Asynchronously get user profile info
        /// </summary>
        /// <param name="id">Id of the user profile</param>
        /// <returns><see cref="UserProfile"/> with information about user</returns>
        Task<UserProfile> GetByIdAsync(string id);
    }
}
