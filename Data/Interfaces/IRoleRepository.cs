﻿using System.Threading.Tasks;

namespace Data.Interfaces
{
    /// <summary>
    /// Interface of role repository
    /// </summary>
    public interface IRoleRepository
    {
        /// <summary>
        /// Asynchronously check if role with passed name exist in database
        /// </summary>
        /// <param name="roleName">Name of the role</param>
        /// <returns>True if such role exists, false if not</returns>
        Task<bool> RoleExistsAsync(string roleName);
    }
}
