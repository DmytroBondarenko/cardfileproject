﻿using System.Threading.Tasks;
using Data.Entities;

namespace Data.Interfaces.BaseInterfaces
{
    /// <summary>
    /// Interface for UOW pattern
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// Text repository
        /// </summary>
        ITextRepository TextRepository { get; }

        /// <summary>
        /// Genre repository
        /// </summary>
        IRepository<Genre> GenreRepository { get; }

        /// <summary>
        /// User repository
        /// </summary>
        IUserRepository UserRepository { get; }

        /// <summary>
        /// User profile repository
        /// </summary>
        IUserProfileRepository UserProfileRepository { get; }

        /// <summary>
        /// Role repository
        /// </summary>
        IRoleRepository RoleRepository { get; }

        /// <summary>
        /// Rating repository
        /// </summary>
        IRepository<Rating> RatingRepository { get; }

        /// <summary>
        /// Save database async
        /// </summary>
        /// <returns>Indication of operation completion</returns>
        Task<int> SaveAsync();
    }
}
