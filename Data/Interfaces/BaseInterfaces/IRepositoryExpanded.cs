﻿using System.Linq;
using System.Threading.Tasks;

namespace Data.Interfaces.BaseInterfaces
{
    /// <summary>
    /// Interface that represents some additional methods to <see cref="IRepository{TEntity}"/>
    /// </summary>
    /// <typeparam name="TEntity">Type of Entity</typeparam>
    public interface IRepositoryExpanded<TEntity> where TEntity : class
    {
        /// <summary>
        /// Gets all entities from context DBSet with details
        /// </summary>
        /// <returns><see cref="IQueryable"/> of all Entities in DBSet</returns>
        Task<IQueryable<TEntity>> GetAllWithDetailsAsync();

        /// <summary>
        /// Gets Entity by its id with details
        /// </summary>
        /// <param name="id">Id of Entity to be found</param>
        /// <returns>Found object</returns>
        Task<TEntity> GetByIdWithDetailsAsync(string id);
    }
}
