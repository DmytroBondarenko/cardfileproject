﻿using System.Linq;
using System.Threading.Tasks;

namespace Data.Interfaces.BaseInterfaces
{
    /// <summary>
    /// Generic repository interface
    /// </summary>
    /// <typeparam name="TEntity">Type of Entity</typeparam>
    public interface IRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// Asynchronously gets all entities from context DBSet
        /// </summary>
        /// <returns><see cref="IQueryable"/> of all Entities in DBSet.</returns>
        Task<IQueryable<TEntity>> GetAllAsync();

        /// <summary>
        /// Gets Entity by its Id
        /// </summary>
        /// <param name="id">Id of Entity to be found</param>
        /// <returns>Found object</returns>
        Task<TEntity> GetByIdAsync(string id);

        /// <summary>
        /// Adds entity to context BDSet
        /// </summary>
        /// <param name="entity">Entity to be added</param>
        Task AddAsync(TEntity entity);

        /// <summary>
        /// Updates Entity
        /// </summary>
        /// <param name="entity">Entity to update</param>
        void Update(TEntity entity);

        /// <summary>
        /// Deletes Entity with specified Id
        /// </summary>
        /// <param name="id">Id of Entity to delete</param>
        void DeleteById(string id);
    }
}
