﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Entities;
using Microsoft.AspNetCore.Identity;

namespace Data.Interfaces
{
    /// <summary>
    /// Interface of user repository
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// Gets all users from context DBSet
        /// </summary>
        /// <returns><see cref="IQueryable"/> of all <see cref="User"/> in DBSet.</returns>
        IQueryable<User> GetAll();

        /// <summary>
        /// Asynchronously get user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns><see cref="User"/> with information about user</returns>
        Task<User> GetByIdAsync(string id);

        /// <summary>
        /// Asynchronously get user by email
        /// </summary>
        /// <param name="email"></param>
        /// <returns><see cref="User"/> with information about user</returns>
        Task<User> GetByEmailAsync(string email);

        /// <summary>
        /// Asynchronously check if passed password matches with passed user
        /// </summary>
        /// <param name="entity">User with which password will be checked</param>
        /// <param name="password">Password</param>
        /// <returns>True if password matches, false if not</returns>
        Task<bool> UserCheckPasswordAsync(User entity, string password);

        /// <summary>
        /// Asynchronously add new use to database
        /// </summary>
        /// <param name="entity">Information about user</param>
        /// <param name="password">User password</param>
        /// <returns><see cref="IdentityResult"/> with the information about operation</returns>
        Task<IdentityResult> UserAddAsync(User entity, string password);

        /// <summary>
        /// Asynchronously update existing user
        /// </summary>
        /// <param name="entity">New information about user</param>
        Task UpdateAsync(User entity);

        /// <summary>
        /// Asynchronously delete user from database
        /// </summary>
        /// <param name="id">Id of the user</param>
        Task DeleteByIdAsync(string id);

        /// <summary>
        /// Asynchronously get user with <see cref="UserProfile"/> included
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns><see cref="User"/> with information about user</returns>
        Task<User> GetByIdWithDetailsAsync(string id);

        /// <summary>
        /// Asynchronously add user to the role
        /// </summary>
        /// <param name="user">User, that should be added to the role</param>
        /// <param name="role">Role to which the user will be added</param>
        /// <returns><see cref="IdentityResult"/> with the information about operation</returns>
        Task<IdentityResult> AddToRoleAsync(User user, string role);

        /// <summary>
        /// Asynchronously get all user roles
        /// </summary>
        /// <param name="user">User of which roles should be returned</param>
        /// <returns><see cref="IEnumerable{T}"/> of user roles</returns>
        Task<IEnumerable<string>> UserGetRolesAsync(User user);

        /// <summary>
        /// Asynchronously change password of the passed user
        /// </summary>
        /// <param name="user">User, of which password will be changed</param>
        /// <param name="currentPassword">Current password</param>
        /// <param name="newPassword">New password</param>
        /// <returns><see cref="IdentityResult"/> with the information about operation</returns>
        Task<IdentityResult> UserChangePasswordAsync(User user, string currentPassword, string newPassword);
    }
}
