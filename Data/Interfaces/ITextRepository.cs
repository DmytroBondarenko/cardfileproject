﻿using Data.Entities;
using Data.Interfaces.BaseInterfaces;

namespace Data.Interfaces
{
    /// <summary>
    /// Interface of text repository
    /// </summary>
    public interface ITextRepository : IRepository<Text>, IRepositoryExpanded<Text>
    {

    }
}
